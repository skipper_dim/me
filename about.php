<?php
    $name = "Дмитрий Палагин";
    $age = 25;
    $mail = "skipper-dim@yandex.ru";
    $city = "Тольятти";
    $txt = "Изучаю PHP";
?>
<!DOCTYPE>
<html lang="ru">
    <head>
        <title><?= $name . ' – ' . $about ?></title>
        <meta charset="utf-8">
        <style>
            body {
                font-family: sans-serif;  
            }
        </style>
    </head>
    <body>
        <h1>Страница пользователя <?= $name ?></h1>
        <dl>
            <dt>Имя:</dt>
            <dd><?= $name ?></dd>
            <dt>Возраст:</dt>
            <dd><?= $age ?></dd>
            <dt>Адрес электронной почты:</dt>
            <dd><a href="mailto:<?= $mail ?>"><?= $mail ?></a></dd>
            <dt>Город:</dt>
            <dd><?= $city ?></dd>
            <dt>О себе:</dt>
            <dd><?= $txt ?></dd>
        </dl>
    </body>
</html>